/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.test;

import in.ac.gpckasargod.student.registration.model.db.Department;
import in.ac.gpckasargod.student.registration.service.DepartmentService;
import in.ac.gpckasargod.student.registration.service.impl.DepartmentServiceImpl;
import java.util.List;


/**
 *
 * @author mkabh
 */
public class DepartmentTest {
    
    public static void main(String args[]){
        DepartmentService departmentService = new DepartmentServiceImpl();
        Department department = new Department(1,"Computer Engineering","CT");
        String message = departmentService.saveDepartment(department);
        System.out.println(message);
        List<Department> departments = departmentService.getAll();
        System.out.println(departments.size());
        
    }
}
