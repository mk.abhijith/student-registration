/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.test;

import in.ac.gpckasargod.student.registration.service.impl.ConnectionServiceImpl;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author mkabh
 */
public class ConnectionTest extends ConnectionServiceImpl{
    
    public static void main(String args[]) throws SQLException{
        ConnectionTest connectionTest = new ConnectionTest();
        Connection connection = connectionTest.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("show databases");
        while(resultSet.next()){
            System.out.println(resultSet.getString("Database"));
        }
    }
}
