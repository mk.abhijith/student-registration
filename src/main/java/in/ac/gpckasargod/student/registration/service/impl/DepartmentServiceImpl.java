/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.service.impl;

import in.ac.gpckasargod.student.registration.constants.MessageConstants;
import in.ac.gpckasargod.student.registration.model.db.Department;
import in.ac.gpckasargod.student.registration.service.DepartmentService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mkabh
 */
public class DepartmentServiceImpl extends ConnectionServiceImpl implements DepartmentService{

    @Override
    public Department[] getAllDepartments() {
        Department[] departments = new Department[2];
        departments[0] = new Department(1,"Computer Engineering","CT");
        departments[1] = new Department(2,"Mechanical Engineering","ME");
        return departments;
    }
    
    @Override
    public String saveDepartment(Department department) {
        String message = "";
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            try {
                String query = "INSERT INTO DEPARTMENT (NAME,SHORT_NAME) VALUES ('" + department.getName() + "','" + department.getShortName() + "')";
                int insert = statement.executeUpdate(query);
                if (insert != 1) {
                    message = MessageConstants.MESSAGE_SAVE_FAILURE;
                } else {
                    message = MessageConstants.MESSAGE_SAVE_SUCCESS;
                }
            } finally {
                connection.close();
                statement.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            message = MessageConstants.MESSAGE_SAVE_FAILURE;
        }
        return message;
    }
    
    public List<Department> getAll(){
        List<Department> departments = new ArrayList<Department>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM DEPARTMENT");
            
            while(resultSet.next()){
                Department department = new Department(resultSet.getInt("ID"),resultSet.getString("NAME"),resultSet.getString("SHORT_NAME"));
                departments.add(department);
            }
        }catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return departments;
    }
    
    public Department readDepartment(Integer id){
        Department department = null;
        try{
            Connection connection = getConnection();
            String query = "SELECT * FROM DEPARTMENT WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            
            while(resultSet.next()){
                department = new Department(resultSet.getInt("ID"),resultSet.getString("NAME"),resultSet.getString("SHORT_NAME"));
                return department;
            }
        }catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return department;
    }
    
    
}
