/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.service;

import in.ac.gpckasargod.student.registration.model.db.Department;
import java.util.List;

/**
 *
 * @author mkabh
 */
public interface DepartmentService {
    
    public Department[] getAllDepartments();     
    public String saveDepartment(Department department);
    public List<Department> getAll();
    public Department readDepartment(Integer id);
}
