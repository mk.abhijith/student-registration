/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.service;

import in.ac.gpckasargod.student.registration.model.db.Registration;
import in.ac.gpckasargod.student.registration.model.ui.RegistrationUIModel;
import java.util.List;

/**
 *
 * @author mkabh
 */
public interface RegistrationService {
    public String saveRegistration(Registration registration);
    public List<RegistrationUIModel> getAll();
    public String deleteRegistration(Integer id);
    public Registration readRegistration(Integer id);
    public String updateRegistration(Registration registration);
}
