/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.constants;

/**
 *
 * @author mkabh
 */
public class ApplicationConstatants {
    public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
    public static final String UI_DATE_FORMAT = "dd-MM-yyyy";
}
