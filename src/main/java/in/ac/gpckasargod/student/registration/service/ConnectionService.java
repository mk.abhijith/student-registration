/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.service;

import java.sql.Connection;

/**
 *
 * @author mkabh
 */
public interface ConnectionService {
    public Connection getConnection();
}
