/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.model.db;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author mkabh
 */
public class Registration {
    private Integer id;
    private String name;
    private String admissionNo;
    private String regNo;
    private Date dob;
    private Integer departmentId;
    private String gender;
    private String emailId;

    public Registration() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmissionNo() {
        return admissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        this.admissionNo = admissionNo;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Registration{" + "id=" + id + ", name=" + name + ", admissionNo=" + admissionNo + ", regNo=" + regNo + ", dob=" + dob + ", departmentId=" + departmentId + ", gender=" + gender + ", emailId=" + emailId + '}';
    }

    
    
}
