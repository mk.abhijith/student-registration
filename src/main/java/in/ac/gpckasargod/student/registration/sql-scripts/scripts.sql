/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  mkabh
 * Created: 2 Apr, 2022
 */

ALTER TABLE REGISTRATION.STUDENT
ADD FOREIGN KEY (DEPT_ID)
REFERENCES REGISTRATION.DEPARTMENT (ID); 

/*ALTER TABLE REGISTRATION.STUDENT
DROP FOREIGN KEY student_ibfk_5;*/

ALTER TABLE REGISTRATION.STUDENT MODIFY COLUMN ID INT AUTO_INCREMENT;

ALTER TABLE REGISTRATION.DEPARTMENT MODIFY COLUMN ID INT AUTO_INCREMENT;