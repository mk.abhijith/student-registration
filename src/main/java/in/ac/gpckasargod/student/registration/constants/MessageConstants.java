/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.constants;

/**
 *
 * @author mkabh
 */
public class MessageConstants {
    public static final String MESSAGE_SAVE_SUCCESS = "Saved successfully";
    public static final String MESSAGE_SAVE_FAILURE = "Save failed";
    public static final String MESSAGE_DELETE_SUCCESS = "Deleted successfully";
    public static final String MESSAGE_DELETE_FAILURE = "Delete failed";
    public static final String MESSAGE_UPDATE_SUCCESS = "Updated successfully";
    public static final String MESSAGE_UPDATE_FAILURE = "Update failed";
}
