/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.service.impl;

import in.ac.gpckasargod.student.registration.service.ConnectionService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mkabh
 */
public class ConnectionServiceImpl implements ConnectionService{

    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/";
    private static final String DATABASE = "registration";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "mysql";
    private static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    
    @Override
    public Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName(DRIVER_NAME);  
            connection = DriverManager.getConnection(CONNECTION_URL+DATABASE, USERNAME, PASSWORD);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }
    
    
    
}
