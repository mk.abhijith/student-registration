/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.ac.gpckasargod.student.registration.service.impl;

import in.ac.gpckasargod.student.registration.constants.ApplicationConstatants;
import in.ac.gpckasargod.student.registration.constants.MessageConstants;
import in.ac.gpckasargod.student.registration.model.db.Registration;
import in.ac.gpckasargod.student.registration.model.ui.RegistrationUIModel;
import in.ac.gpckasargod.student.registration.service.RegistrationService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mkabh
 */
public class RegistrationServiceImpl extends ConnectionServiceImpl implements RegistrationService{
   public Registration[] getAllRegistrations(){
        Registration[] registrations = new Registration[10];
        Registration registration = new Registration();
        registration.setName("Abhijith M K");
        registration.setAdmissionNo("11111");
        registration.setRegNo("1");
        registration.setEmailId("abhijithmk@gpckasaragod.ac.in");
        registration.setDob(new Date());
        registration.setGender("M");
        registration.setDepartmentId(1);
        return registrations;
    }
   
   public String saveRegistration(Registration registration){
       SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstatants.DB_DATE_FORMAT);
       String message = "";
        try {
            Connection connection = getConnection();
            //String query = "INSERT INTO STUDENT (NAME,ADMISSION_NO,REG_NO,DOB,GENDER,EMAIL_ID,DEPT_ID) VALUES ('?','?','?','?','?','?',?)";
            String query = "INSERT INTO STUDENT (NAME,ADMISSION_NO,REG_NO,DOB,GENDER,EMAIL_ID,DEPT_ID) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, registration.getName());
            statement.setString(2, registration.getAdmissionNo());
            statement.setString(3, registration.getRegNo());
            statement.setString(4, dateFormat.format(registration.getDob()));
            statement.setString(5, registration.getGender());
            statement.setString(6, registration.getEmailId());
            statement.setInt(7, registration.getDepartmentId());
            try {
                
                int insert = statement.executeUpdate();
                if (insert != 1) {
                    message = MessageConstants.MESSAGE_SAVE_FAILURE;
                } else {
                    message = MessageConstants.MESSAGE_SAVE_SUCCESS;
                }
            } finally {
                connection.close();
                statement.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            message = MessageConstants.MESSAGE_SAVE_FAILURE;
        }
        return message;
   }
   
   public List<RegistrationUIModel> getAll(){
       List<RegistrationUIModel> registrations = new ArrayList<>();
        try {
            SimpleDateFormat uiDateFormat = new SimpleDateFormat(ApplicationConstatants.UI_DATE_FORMAT);
            SimpleDateFormat dbDateFormat = new SimpleDateFormat(ApplicationConstatants.DB_DATE_FORMAT);
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT S.ID,S.NAME,S.ADMISSION_NO,S.REG_NO,S.DOB,S.GENDER,S.EMAIL_ID,D.NAME AS DEPT_NAME FROM STUDENT S INNER JOIN DEPARTMENT D ON S.DEPT_ID=D.ID ORDER BY S.ID");
            while(resultSet.next()){
                RegistrationUIModel registration = new RegistrationUIModel();
                registration.setId(resultSet.getInt("ID"));
                registration.setName(resultSet.getString("NAME"));
                registration.setAdmissionNo(resultSet.getString("ADMISSION_NO"));
                registration.setRegNo(resultSet.getString("REG_NO"));
                registration.setGender(resultSet.getString("GENDER"));
                registration.setEmailId(resultSet.getString("EMAIL_ID"));
                registration.setDepartment(resultSet.getString("DEPT_NAME"));
                String dob = resultSet.getString("DOB");
                registration.setDob(uiDateFormat.format(dbDateFormat.parse(dob)));
                registrations.add(registration);
            }
        }catch (SQLException ex) {
            Logger.getLogger(RegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
           
        }catch (Exception ex){
            Logger.getLogger(RegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
       return registrations;
   }
   
   public String deleteRegistration(Integer id){
       SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstatants.DB_DATE_FORMAT);
       String message = "";
        try {
            Connection connection = getConnection();
            //String query = "INSERT INTO STUDENT (NAME,ADMISSION_NO,REG_NO,DOB,GENDER,EMAIL_ID,DEPT_ID) VALUES ('?','?','?','?','?','?',?)";
            String query = "DELETE FROM STUDENT WHERE ID=?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            try {
                
                int deleted = statement.executeUpdate();
                if (deleted != 1) {
                    message = MessageConstants.MESSAGE_DELETE_FAILURE;
                } else {
                    message = MessageConstants.MESSAGE_DELETE_SUCCESS;
                }
            } finally {
                connection.close();
                statement.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(RegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            message = MessageConstants.MESSAGE_DELETE_FAILURE;
        }
        return message;
   }
   
   public String updateRegistration(Registration registration){
       String message = "";
       try{
           SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstatants.DB_DATE_FORMAT);
           Connection connection = getConnection();
           String query = "UPDATE STUDENT SET NAME=?,ADMISSION_NO=?,REG_NO=?,DOB=?,GENDER=?,EMAIL_ID=?,DEPT_ID=? WHERE ID=?";
           PreparedStatement statement = connection.prepareStatement(query);
           statement.setString(1, registration.getName());
           statement.setString(2, registration.getAdmissionNo());
           statement.setString(3, registration.getRegNo());
           statement.setString(4, dateFormat.format(registration.getDob()));
           statement.setString(5, registration.getGender());
           statement.setString(6, registration.getEmailId());
           statement.setInt(7, registration.getDepartmentId());
           statement.setInt(8, registration.getId());
           try{
               int updated = statement.executeUpdate();
               if (updated != 1) {
                    message = MessageConstants.MESSAGE_UPDATE_FAILURE;
                } else {
                    message = MessageConstants.MESSAGE_UPDATE_SUCCESS;
                }
           }finally{
               connection.close();
               statement.close();
           }
       }catch(Exception ex){
           Logger.getLogger(RegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            message = MessageConstants.MESSAGE_UPDATE_FAILURE;
       }
       return message;
   }
   
   public Registration readRegistration(Integer id){
       Registration registration = null;
       try{
           SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstatants.DB_DATE_FORMAT);
           Connection connection = getConnection();
           String query = "SELECT * FROM STUDENT WHERE ID=?"; 
           PreparedStatement statement = connection.prepareStatement(query);
           statement.setInt(1, id);
           ResultSet resultSet = statement.executeQuery();
           //if(resultSet.getRow() != 0)
            registration = new Registration();
           while(resultSet.next()){
            registration.setId(resultSet.getInt("ID"));
            registration.setName(resultSet.getString("NAME"));
            registration.setAdmissionNo(resultSet.getString("ADMISSION_NO"));
            registration.setRegNo(resultSet.getString("REG_NO"));
            registration.setGender(resultSet.getString("GENDER"));
            registration.setEmailId(resultSet.getString("EMAIL_ID"));
            registration.setDepartmentId(resultSet.getInt("DEPT_ID"));
            String dob = resultSet.getString("DOB");
            registration.setDob(dateFormat.parse(dob));
           }
       }catch(Exception ex){
           Logger.getLogger(RegistrationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
       }
       return registration;
   }
   
   
}
